import React from 'react';

const Showings = props => {
    return (
        <tr>
            <td className=''>{props.showings.title} </td>
            <td className=''>{props.showings.showingTimes}</td>
            <td className=''><img src={props.showings.img} alt={props.showings.title} /></td>
        </tr>
    )
};

export default Showings;