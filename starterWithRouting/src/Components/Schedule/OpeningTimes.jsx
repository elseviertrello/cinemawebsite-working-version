import React from 'react';

const OpeningTimes = props => {
    return (
        <tr>
            <td className=''>{props.openingtimes.day} </td>
            <td className=''>{props.openingtimes.opening} - {props.openingtimes.close}</td>
        </tr>
    )
};

export default OpeningTimes;