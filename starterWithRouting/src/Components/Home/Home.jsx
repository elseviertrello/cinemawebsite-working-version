import React from 'react';
import { CONTENTARTICLETEXT } from '../../js/constants/contentArticleText.js';
import { HOMEIMAGES } from '../../js/constants/homeImage.js';

const Home = () => {
    return (
        <div className='container'>

            <img className='w-100' src={HOMEIMAGES[0].src} alt={HOMEIMAGES[0].alt} />

            {/* note className 'my-3' adds margin top and bottom */}

            <p className='my-3'> {CONTENTARTICLETEXT} </p>

            {/* note className 'col-sm-4' adds responsive column of 1/3 total width */}

            <div className="row">
                <img className="col-sm-4 my-3" src={HOMEIMAGES[1].src} alt={HOMEIMAGES[1].alt} />
                <img className="col-sm-4 my-3" src={HOMEIMAGES[2].src} alt={HOMEIMAGES[2].alt} />
                <img className="col-sm-4 my-3" src={HOMEIMAGES[1].src} alt={HOMEIMAGES[1].alt} />
            </div>

            <p className='my-3'> {CONTENTARTICLETEXT} </p>

        </div>
    );
}

export default Home;