const mongoose = require("mongoose");

const ShowingsSchema = mongoose.Schema({
    title: String,
    synopsis: String,
    cast: String,
    directors: String,
    showingTimes: String,
    releaseDate: Date,
    filmStatus: Boolean,
    img: String
});

module.exports = mongoose.model("showings", ShowingsSchema);