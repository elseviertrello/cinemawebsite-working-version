const mongoose = require("mongoose");

const OpeningTimesSchema = mongoose.Schema(
    {
        day: String,
        opening: String,
        close: String
    }
);

module.exports = mongoose.model("OpeningTimes", OpeningTimesSchema)