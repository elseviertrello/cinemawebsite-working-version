const express = require("express");
const mongo = require('mongoose');
const cors = require('cors');
const PORT = 8080;
const openings = require("./routes/Openings")
const Showings = require("./routes/Showings")
const signUp = require("./routes/signUp");
const app = express();
const bodyParser = require("body-parser");

app.use(cors());
app.use(`/Openings`, openings);
app.use(`/Showings`, Showings);
app.use(`/signUp`, signUp);

app.use(bodyParser.json())

mongo.connect("mongodb://mymongo:27017/cinema")
    .then(() => console.log("Successfully connected to the database"))
    .catch(err => console.log("well, that didn't work " + err));

const server = app.listen(PORT, '0.0.0.0', () => {
    const SERVERHOST = server.address().address;
    console.log(`Example app listening at
    http://${SERVERHOST}:${PORT}`);
});